package com.noah2021.service;

import com.noah2021.error.BusinessException;
import com.noah2021.service.model.ItemModel;

import java.util.List;

public interface ItemService {

    //创建商品
    public ItemModel createItem(ItemModel itemModel) throws BusinessException;

    //商品列表浏览
    public List<ItemModel> listItem();

    //商品详情浏览
    public ItemModel getItemById(Integer id);

    //库存扣减
    boolean decreaseStock(Integer itemId, Integer amount);

    //库存回补
    boolean increaseStock(Integer itemId, Integer amount);

    //销量增加
    void increaseSales(Integer itemId, Integer amount);

    //item和promo model缓存模型:验证商品和活动是否有效
    ItemModel getItemByIdInCache(Integer id);

    //异步更新库存
    boolean asyncDecreaseStock(Integer itemId, Integer amount);

    //初始化库存流水
    String initStockLog(Integer itemId, Integer amount);
}
