package com.noah2021.service;

import com.noah2021.service.model.PromoModel;

public interface PromoService {
    //获取将来进行或当前进行的秒杀活动
    PromoModel getPromoByItemId(Integer itemId);

    //活动发布同步库存进缓存
    void publishPromo(Integer promoId);

    //生成秒杀令牌
    String generateSecondKillToken(Integer promoId,Integer itemId,Integer userId);
}
