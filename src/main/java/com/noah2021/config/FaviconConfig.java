package com.noah2021.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 〈解决报错：NoHandlerFoundException: No handler found for GET /favicon.ico〉
 *  这个报错只是显示在控制台，不处理其实也不影响
 * @author Noah2021
 * @create 2022/3/28
 * @return
 */
@Configuration
public class FaviconConfig extends WebMvcConfigurerAdapter {
    /**
     * 添加处理程序以提供来自 Web 应用程序根目录、类路径等特定位置的静态资源，例如图像、js 和 css 文件。
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/favicon.ico").addResourceLocations("classpath:/htmlStable/static/");
    }
}
